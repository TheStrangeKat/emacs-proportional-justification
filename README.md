# Emacs Proportional Justification

Modifies existing justification functions to handle proportional fonts in a better way.